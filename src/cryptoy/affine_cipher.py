from math import (
    gcd,
)

from cryptoy.utils import (
    str_to_unicodes,
    unicodes_to_str,
)

# TP: Chiffrement affine


def compute_permutation(a: int, b: int, n: int) -> list[int]:
    # A implémenter, en sortie on doit avoir une liste result tel que result[i] == (a * i + b) % n
    result = []
    if gcd(a, n) == 0:
        raise RuntimeError
        exit()
    for i in range (n):
        result.append((a * i + b) % n)
    
    return(result)

def compute_inverse_permutation(a: int, b: int, n: int) -> list[int]:
    # A implémenter, pour cela on appelle perm = compute_permutation(a, b, n) et on calcule la permutation inverse
    # result qui est telle que: perm[i] == j implique result[j] == i
    result = [0]*n
    
    perm = compute_permutation(a,b,n)
    for i,val in enumerate(perm):
        result[val] = i

    return result


def encrypt(msg: str, a: int, b: int) -> str:
    # A implémenter, en utilisant compute_permutation, str_to_unicodes et unicodes_to_str
    unicode_msg = str_to_unicodes(msg)
    perm = compute_permutation(a, b, 0x110000)

    for i in range (len(unicode_msg)) :
        unicode_msg[i] = perm[unicode_msg[i]]
   
    return unicodes_to_str(unicode_msg)

def encrypt_optimized(msg: str, a: int, b: int) -> str:
    # A implémenter, sans utiliser compute_permutation
    encrypted_message = []
    unicode_msg = str_to_unicodes(msg)
    for char in unicode_msg:
        encrypted_code = (a * char + b) % 0x110000
        encrypted_message.append(encrypted_code)
    return unicodes_to_str(encrypted_message)


def decrypt(msg: str, a: int, b: int) -> str:
    # A implémenter, en utilisant compute_inverse_permutation, str_to_unicodes et unicodes_to_str
    unicode_msg=str_to_unicodes(msg)
    perm = compute_inverse_permutation(a, b, 0x110000)

    for i in range (len(unicode_msg)):
        unicode_msg[i] = perm[unicode_msg[i]]

    return unicodes_to_str(unicode_msg)


def decrypt_optimized(msg: str, a_inverse: int, b: int) -> str:
    # A implémenter, sans utiliser compute_inverse_permutation
    # On suppose que a_inverse a été précalculé en utilisant compute_affine_key_inverse, et passé
    # a la fonction

    decrypted_message = []
    unicode_msg = str_to_unicodes(msg)
    for char in unicode_msg:
        decrypted_code = (a_inverse * (char - b)) % 0x110000
        decrypted_message.append(decrypted_code)
    return unicodes_to_str(decrypted_message)


def compute_affine_keys(n: int) -> list[int]:
    # A implémenter, doit calculer l'ensemble des nombre a entre 1 et n tel que gcd(a, n) == 1
    # c'est à dire les nombres premiers avec n
    res = []
    for a in range(1,n):
        if (gcd(a, n) == 1):
            res.append(a)

    return res

def compute_affine_key_inverse(a: int, affine_keys: list, n: int) -> int:
    # Trouver a_1 dans affine_keys tel que a * a_1 % N == 1 et le renvoyer
    # Placer le code ici (une boucle)
    for i in affine_keys:
        if (a * i % n == 1):
            return i
    # Si a_1 n'existe pas, alors a n'a pas d'inverse, on lance une erreur:
    raise RuntimeError(f"{a} has no inverse")


def attack() -> tuple[str, tuple[int, int]]:
    s = "࠾ੵΚઐ௯ஹઐૡΚૡೢఊஞ௯\u0c5bૡీੵΚ៚Κஞїᣍફ௯ஞૡΚր\u05ecՊՊΚஞૡΚՊեԯՊ؇ԯրՊրր"
    # trouver msg, a et b tel que affine_cipher_encrypt(msg, a, b) == s
    # avec comme info: "bombe" in msg et b == 58

    
    a=1
    found = False
    while not found:
        decrypted = decrypt(s, a, 58)
        print(decrypted)
        if "bombe" in decrypted :
            found = True
            return decrypted, (a, 58)
        else:
            a += 1

    if not found:
        raise RuntimeError("Failed to attack")

    raise RuntimeError("Failed to attack")


def attack_optimized() -> tuple[str, tuple[int, int]]:
    s = (
        "જഏ൮ൈ\u0c51ܲ೩\u0c51൛൛అ౷\u0c51ܲഢൈᘝఫᘝా\u0c51\u0cfc൮ܲఅܲᘝ൮ᘝܲాᘝఫಊಝ"
        "\u0c64\u0c64ൈᘝࠖܲೖఅܲఘഏ೩ఘ\u0c51ܲ\u0c51൛൮ܲఅ\u0cfc\u0cfcඁೖᘝ\u0c51"
    )
    # trouver msg, a et b tel que affine_cipher_encrypt(msg, a, b) == s
    # avec comme info: "bombe" in msg
    msg = ""
    possibilites = compute_affine_keys(0x110000)
    for a in range(1,len(possibilites)):
        try:
            a_inverse = compute_affine_key_inverse(a, possibilites, 0x110000)
        except RuntimeError:
            continue
        for b in range(1,15000):
            msg = decrypt_optimized(s, a_inverse, b)
            if "bombe" in msg:
                return (msg, (a, b))
        
    raise RuntimeError("Failed to attack")
    
